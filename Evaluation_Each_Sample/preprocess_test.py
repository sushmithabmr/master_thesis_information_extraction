import argparse
import json
import os

from PIL import Image
from transformers import AutoTokenizer


def bbox_string(box, width, length):
    return (
        str(int(1000 * (box[0] / width)))
        + " "
        + str(int(1000 * (box[1] / length)))
        + " "
        + str(int(1000 * (box[2] / width)))
        + " "
        + str(int(1000 * (box[3] / length)))
    )


def actual_bbox_string(box, width, length):
    return (
        str(box[0])
        + " "
        + str(box[1])
        + " "
        + str(box[2])
        + " "
        + str(box[3])
        + "\t"
        + str(width)
        + " "
        + str(length)
    )


def convert(args):
    #data_dir = '/content/drive/MyDrive/SushThesis/unilm/layoutlm/examples/seq_labeling/dataset1/testing_data/test/82092117'
    #output_dir = '/content/drive/MyDrive/SushThesis/unilm/layoutlm/examples/seq_labeling/dataset1/testing_data/test/82092117'
    itercars = iter(os.walk(args.data_dir))
    next(itercars)
    for subdir, dirs, files in itercars:
        #print(subdir)
    #for subdir, dirs, files in os.walk(args.data_dir):

        #print(subdir)    
        with open(
            os.path.join(subdir, args.data_split + ".txt.tmp"),
            "w",
            encoding="utf8",
        ) as fw, open(
            os.path.join(subdir, args.data_split + "_box.txt.tmp"),
            "w",
            encoding="utf8",
        ) as fbw, open(
            os.path.join(subdir, args.data_split + "_image.txt.tmp"),
            "w",
            encoding="utf8",
        ) as fiw:
        #for file in os.listdir(args.data_dir):
            #file_path = os.path.join(args.data_dir)
            #file_path = data_dir
            #print(file_path)
            fol = subdir.split('/')[-1]
            #print(fol)
            image_path = os.path.join(subdir,fol+".png")
            #print("image_path : ",image_path)
            json_path = os.path.join(subdir,fol+".json")
            #print("json_path : ", json_path)
            with open(json_path, "r", encoding="utf8") as f:
                data = json.load(f)
            #image_path = file_path.replace("annotations", "images")
            #image_path = file_path
            #image_path = image_path.replace("json", "png")
            #print(file_path)
            #image_path = file_path
            file_name = os.path.basename(image_path)
            image = Image.open(image_path)
            width, length = image.size
            for item in data["form"]:
                words, label = item["words"], item["label"]
                words = [w for w in words if w["text"].strip() != ""]
                if len(words) == 0:
                    continue
                if label == "other":
                    for w in words:
                        fw.write(w["text"] + "\tO\n")
                        fbw.write(
                            w["text"]
                            + "\t"
                            + bbox_string(w["box"], width, length)
                            + "\n"
                        )
                        #print("other" + bbox_string(w["box"], width, length))
                        fiw.write(
                            w["text"]
                            + "\t"
                            + actual_bbox_string(w["box"], width, length)
                            + "\t"
                            + file_name
                            + "\n"
                        )
                        #print("other_actual" + actual_bbox_string(w["box"], width, length))
                else:
                    if len(words) == 1:
                        fw.write(words[0]["text"] + "\tS-" + label.upper() + "\n")
                        fbw.write(
                            words[0]["text"]
                            + "\t"
                            + bbox_string(words[0]["box"], width, length)
                            + "\n"
                        )
                        #print("len 1 bbox" + bbox_string(words[0]["box"], width, length))
                        fiw.write(
                            words[0]["text"]
                            + "\t"
                            + actual_bbox_string(words[0]["box"], width, length)
                            + "\t"
                            + file_name
                            + "\n"
                        )
                        #print("len 1 actual bbox" + actual_bbox_string(words[0]["box"], width, length))
                    else:
                        fw.write(words[0]["text"] + "\tB-" + label.upper() + "\n")
                        fbw.write(
                            words[0]["text"]
                            + "\t"
                            + bbox_string(words[0]["box"], width, length)
                            + "\n"
                        )
                        #print("words[0] else bbox" + bbox_string(words[0]["box"], width, length))
                        fiw.write(
                            words[0]["text"]
                            + "\t"
                            + actual_bbox_string(words[0]["box"], width, length)
                            + "\t"
                            + file_name
                            + "\n"
                        )
                        #print("words[0] actual bbox" + actual_bbox_string(words[0]["box"], width, length))
                        for w in words[1:-1]:
                            fw.write(w["text"] + "\tI-" + label.upper() + "\n")
                            fbw.write(
                                w["text"]
                                + "\t"
                                + bbox_string(w["box"], width, length)
                                + "\n"
                            )
                            #print("words[1:-1] else bbox" + bbox_string(words[0]["box"], width, length))
                            fiw.write(
                                w["text"]
                                + "\t"
                                + actual_bbox_string(w["box"], width, length)
                                + "\t"
                                + file_name
                                + "\n"
                            )
                            #print("words[1:-1] actual bbox" + actual_bbox_string(words[0]["box"], width, length))
                        fw.write(words[-1]["text"] + "\tE-" + label.upper() + "\n")
                        fbw.write(
                            words[-1]["text"]
                            + "\t"
                            + bbox_string(words[-1]["box"], width, length)
                            + "\n"
                        )
                        #print("words[-1] else bbox" + bbox_string(words[0]["box"], width, length)
                        fiw.write(
                            words[-1]["text"]
                            + "\t"
                            + actual_bbox_string(words[-1]["box"], width, length)
                            + "\t"
                            + file_name
                            + "\n"
                        )
                        #print("words[-1] else actual bbox" +  actual_bbox_string(words[-1]["box"], width, length))

            fw.write("\n")
            fbw.write("\n")
            fiw.write("\n")


def seg_file(file_path, tokenizer, max_len):
    subword_len_counter = 0
    output_path = file_path[:-4]
    with open(file_path, "r", encoding="utf8") as f_p, open(
        output_path, "w", encoding="utf8"
    ) as fw_p:
        for line in f_p:
            line = line.rstrip()

            if not line:
                fw_p.write(line + "\n")
                subword_len_counter = 0
                continue
            token = line.split("\t")[0]

            current_subwords_len = len(tokenizer.tokenize(token))

            # Token contains strange control characters like \x96 or \x95
            # Just filter out the complete line
            if current_subwords_len == 0:
                continue

            if (subword_len_counter + current_subwords_len) > max_len:
                fw_p.write("\n" + line + "\n")
                subword_len_counter = current_subwords_len
                continue

            subword_len_counter += current_subwords_len

            fw_p.write(line + "\n")


def seg(args):
    tokenizer = AutoTokenizer.from_pretrained(
        args.model_name_or_path, do_lower_case=True
    )
    itercars = iter(os.walk(args.data_dir))
    next(itercars)
    for subdir, dirs, files in itercars:
        #print(subdir)
        seg_file(
            os.path.join(subdir, args.data_split + ".txt.tmp"),
            tokenizer,
            args.max_len,
        )
        seg_file(
            os.path.join(subdir, args.data_split + "_box.txt.tmp"),
            tokenizer,
            args.max_len,
        )
        seg_file(
            os.path.join(subdir, args.data_split + "_image.txt.tmp"),
            tokenizer,
            args.max_len,
        )


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--data_dir", type=str, default="data/training_data/annotations"
    )
    parser.add_argument("--data_split", type=str, default="train")
    parser.add_argument("--output_dir", type=str, default="data")
    parser.add_argument("--model_name_or_path", type=str, default="bert-base-uncased")
    parser.add_argument("--max_len", type=int, default=510)
    args = parser.parse_args()

    convert(args)
    seg(args)