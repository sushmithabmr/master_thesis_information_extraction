#Evaluation
from torch.utils.data import Dataset, DataLoader
import os
import numpy as np
import torch
from torchvision.transforms import ToTensor
from transformers import AdamW
from tqdm.notebook import tqdm
import torch
from preprocess_images import *
from transformers import BertTokenizer
from data import FUNSDDataset
from model import * 
from seqeval.metrics import (
    classification_report,
    f1_score,
    precision_score,
    recall_score,
)


def main():

    eval_loss = 0.0
    nb_eval_steps = 0
    preds = None
    out_label_ids = None

    tokenizer = BertTokenizer.from_pretrained("bert-base-uncased")
    image_files_test = [f for f in os.listdir('/content/drive/MyDrive/SushThesis/Image_Embedding_for_LayoutLM/dataset/testing_data/images')]
    test_dataset = FUNSDDataset(image_file_names=image_files_test, tokenizer=tokenizer, max_length=512, target_size=224, train=False)
    test_dataloader = DataLoader(test_dataset, batch_size=4)

    # Then later:
    model = LayoutLMForTokenClassification()
    state_dict = torch.load('/content/drive/MyDrive/SushThesis/Image_Embedding_for_LayoutLM/model.pt')
    model.load_state_dict(state_dict)
    #model = torch.load_state_dict(torch.load('/content/drive/MyDrive/SushThesis/Image_Embedding_for_LayoutLM/model.pt'))
    model.eval()
    for batch in tqdm(test_dataloader, desc="Evaluating"):
        with torch.no_grad():
            input_ids=batch['input_ids']
            bbox=batch['bbox']
            attention_mask=batch['attention_mask']
            token_type_ids=batch['token_type_ids']
            labels=batch['labels']
            resized_images = batch['resized_image'] 
            resized_and_aligned_bounding_boxes = batch['resized_and_aligned_bounding_boxes'] 

            # forward pass
            outputs = model(input_ids=input_ids, bbox=bbox, attention_mask=attention_mask, token_type_ids=token_type_ids, 
                            labels=labels, resized_images=resized_images, resized_and_aligned_bounding_boxes=resized_and_aligned_bounding_boxes)

            # get the loss and logits
            tmp_eval_loss = outputs.loss
            logits = outputs.logits

            eval_loss += tmp_eval_loss.item()
            nb_eval_steps += 1

            # compute the predictions
            if preds is None:
                preds = logits.detach().cpu().numpy()
                out_label_ids = labels.detach().cpu().numpy()
            else:
                preds = np.append(preds, logits.detach().cpu().numpy(), axis=0)
                out_label_ids = np.append(
                    out_label_ids, labels.detach().cpu().numpy(), axis=0
                )

    # compute average evaluation loss
    eval_loss = eval_loss / nb_eval_steps
    preds = np.argmax(preds, axis=2)

    out_label_list = [[] for _ in range(out_label_ids.shape[0])]
    preds_list = [[] for _ in range(out_label_ids.shape[0])]

    labels = ['B-answer', 'I-answer', 'B-header', 'I-header', 'B-question', 'I-question', 'B-other', 'I-other']
    idx2label = {v: k for v,k in enumerate(labels)}
    label2idx = {k: v for v,k in enumerate(labels)}

    for i in range(out_label_ids.shape[0]):
        for j in range(out_label_ids.shape[1]):
            if out_label_ids[i, j] != -100:
                out_label_list[i].append(idx2label[out_label_ids[i][j]])
                preds_list[i].append(idx2label[preds[i][j]])

    results = {
        "loss": eval_loss,
        "precision": precision_score(out_label_list, preds_list),
        "recall": recall_score(out_label_list, preds_list),
        "f1": f1_score(out_label_list, preds_list),
    }
    print(results)

if __name__ == "__main__" :
 #   print("hello")
    main()

