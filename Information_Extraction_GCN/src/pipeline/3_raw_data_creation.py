#!/usr/bin/python
import os
import sys
import shutil
import configparser
import argparse
import util
import json
import numpy as np
import pandas as pd


def main():

    # set up a parser
    parser = argparse.ArgumentParser()
    print("Raw Data Creation")
    parser.add_argument("-c", "--config", required=False, default="config.ini", help="Config file")

    # parse arguments
    args = parser.parse_args()
    cfg = args.config
    cfgparser = configparser.ConfigParser()
    res = cfgparser.read(cfg)
    if len(res) == 0:
        print("Error: None of the config files could be read")
        sys.exit(1)

    # read the config
    # Raw data prepared only for training data in the FUNSD actual dataset. 
    # The same can be performed to also testing_data folder.

    train_data_path = util.read_cfg_string(cfgparser, 'input', 'train_data_path', default=None)
    # test_data_path = util.read_cfg_string(cfgparser, 'input', 'test_data_path', default=None)

    raw_data_output_folder = util.read_cfg_string(cfgparser, 'output', 'raw_data_path', default=None)
    # processed_data_output_folder = util.read_cfg_string(cfgparser,'output','processed_data_path',default=None)


    # To list files inside FUNSD/training_data/each_sample_folder
    samples = os.listdir(os.path.join(train_data_path,'each_sample_folder'))
    print(samples)

    # Checks img and box folders exist for raw data creation to train GCN
    if not os.path.exists(os.path.join(raw_data_output_folder,'img')) and not os.path.exists(os.path.join(raw_data_output_folder,'box')):
        os.mkdir(os.path.join(raw_data_output_folder,'img'))
        os.mkdir(os.path.join(raw_data_output_folder,'box'))
    else:
        for num in range(len(samples)):
            name = samples[num].split('.')[0]
            # copy image from each folder to 'raw/img' folder
            src_img = os.path.join(train_data_path,'each_sample_folder',name,name+".png")
            #print(src_img)
            dest = os.path.join(raw_data_output_folder,'img',name+".png")

            # To move each sample's image from respective folders  to  img folder
            shutil.copyfile(src_img,dest)


            # read bounding boxes and words from train_image.txt and respective labels from train.txt as dataframe
            df_raw  = pd.read_csv(os.path.join(train_data_path,'each_sample_folder',name,"train_image.txt"),sep='\\t', names=['Object', 'box', 'img-size', 'img-name'], encoding='utf-8', engine='python')
            df_label  = pd.read_csv(os.path.join(train_data_path,'each_sample_folder',name,"train.txt"),sep='\\t', names=['Object', 'label'], encoding='utf-8', engine='python')

            # save "xmin", "ymin", "xmax", "ymax", "Object", "label" to csv file in 'raw/box' folder of each sample
            df_raw[["xmin", "ymin", "xmax", "ymax"]] = df_raw['box'].str.split(' ', expand=True)
            df_raw = df_raw.drop(['box', 'img-size', 'img-name'], axis=1)
            df_raw = df_raw[["xmin", "ymin", "xmax", "ymax", "Object"]]

            df_raw[['label']] = df_label[['label']]
            # Save each sample's dataframe to raw/box folder
            df_raw.to_csv(os.path.join(raw_data_output_folder, 'box', name+'.csv'), index=False)
            print("Raw data training files saved in folder: ", os.path.join(raw_data_output_folder, 'box', name))


if __name__ == "__main__":
    main()
