import os.path as osp
import argparse
import numpy as np 
import pandas as pd
import torch
import torch.nn.functional as F
import torch_geometric.transforms as T
from torch_geometric.nn import GCNConv, ChebConv  # noqa
from torch.nn import Parameter
from sklearn.utils import class_weight
from sklearn.metrics import confusion_matrix, f1_score

#from seqeval.metrics import (classification_report,f1_score, precision_score,recall_score)


data_path = "../../data/test/processed/" + "data_with_texts_inference.dataset"
df = pd.read_csv("../../data/processed/test/processed_data.csv")
data = torch.load(data_path)

print(f'testing nodes: {data.y[data.test_mask].shape}')


print(f'number of nodes: {data.x.shape}')

parser = argparse.ArgumentParser()

parser.add_argument('--model', type=str, default ='ChebConv',
                    help = 'GCN or ChebConv model')
parser.add_argument('--verbose', type=int, default=0,
                    help='print confusion matrix')
args = parser.parse_args()

class Net(torch.nn.Module):
    def __init__(self):
        super(Net, self).__init__()
        if args.model == 'GCN':
            #cached = True is for transductive learning
            self.conv1 = GCNConv(data.x.shape[1], 16, cached=True)
            self.conv2 = GCNConv(16, 32, cached=True)  
            self.conv3 = GCNConv(32, 64, cached=True) 
            self.conv4 = GCNConv(64, 13, cached=True)   
        elif args.model == 'ChebConv': 
            self.conv1 = ChebConv(data.x.shape[1], 16, K=3)
            self.conv2 = ChebConv(16, 32, K=3)
            self.conv3 = ChebConv(32, 64, K=3)
            self.conv4 = ChebConv(64, 13, K=3)


    def forward(self):
        x, edge_index, edge_weight = data.x, data.edge_index, data.edge_attr

        x = F.relu(self.conv1(x, edge_index))
        x = F.relu(self.conv2(x, edge_index))
        x = F.relu(self.conv3(x, edge_index))
        x = self.conv4(x, edge_index)

        return F.log_softmax(x, dim=1)


#device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
device = torch.device('cpu')
model, data = Net().to(device), data.to(device)
model.load_state_dict(torch.load("../../data/processed/model.pt"))


def inference():
    model.eval()
    #F.nll_loss(model()[data.val_mask], data.y[data.val_mask])
    logits, accs = model(), []
    for mask_name, mask in data('test_mask'):
        #print("mask_name:", mask_name)
        #print("mask:", mask)
        pred = logits[mask].max(1)[1]
        print("Pred_shape", pred.shape)
        acc = pred.eq(data.y[mask]-1).sum().item() / mask.sum().item()  
        if args.verbose == 1:
            # r"printing predicted classes and number of elements for preds"
            #print('pred', pred)
            pred_df = pd.DataFrame(pred, columns=['pred']) 
            #df['labels'] = df['labels'].fillna('undefined')
            pred_df.loc[pred_df['pred'] == 12 ] = 'O'
            pred_df.loc[pred_df['pred'] == 0 ] = 'B-HEADER'
            pred_df.loc[pred_df['pred'] == 1 ] = 'I-HEADER'
            pred_df.loc[pred_df['pred'] == 2 ] = 'E-HEADER'
            pred_df.loc[pred_df['pred'] == 3 ] = 'S-HEADER'
            pred_df.loc[pred_df['pred'] == 4 ] = 'B-ANSWER'
            pred_df.loc[pred_df['pred'] == 5 ] = 'I-ANSWER'
            pred_df.loc[pred_df['pred'] == 6 ] = 'E-ANSWER'
            pred_df.loc[pred_df['pred'] == 7 ] = 'S-ANSWER'
            pred_df.loc[pred_df['pred'] == 8 ] = 'B-QUESTION'
            pred_df.loc[pred_df['pred'] == 9 ] = 'I-QUESTION'
            pred_df.loc[pred_df['pred'] == 10 ] = 'E-QUESTION'
            pred_df.loc[pred_df['pred'] == 11 ] = 'S-QUESTION'
            df['pred_num'] = pred + 1
            df['pred'] = pred_df['pred']
            df.to_csv("../../data/test/processed/inference_data.csv")
            #print(df)
            unique_elements, counts_elements = np.unique(pred, return_counts=True)
            print(np.asarray((unique_elements, counts_elements)))

            # r"printing predicted classes and number of elements for actual"
            print('actual')
            unique_elements, counts_elements = np.unique((data.y[mask]-1), return_counts=True)
            print(np.asarray((unique_elements, counts_elements)))
        #confusion matrix
            if mask_name == 'test_mask':
                conf_mat=confusion_matrix((data.y[mask]-1).numpy(), pred.numpy())
                #print(f'confusion_matrix: \n   {conf_mat}')
                actual = (data.y[mask]-1).numpy().astype(str)
                predictions = pred.numpy().astype(str)
                print(actual)
                print(predictions)
                f1 = f1_score(actual, predictions, average='micro')
                print(f'F1 Score: \n   {f1}')
                #report = classification_report(actual, predictions)
                #print(f'report: \n   {report}')
                class_accuracy=100*conf_mat.diagonal()/conf_mat.sum(1)
                print(class_accuracy)
        accs.append(acc)
    return accs

if __name__ == '__main__':
    test_acc = inference()
    print(test_acc)

