## Table of contents
* [OCR_and_ActiveLearning_For_LayoutLM](#OCR_and_ActiveLearning_For_LayoutLM)
* [Prerequisites](#prerequisites)
* [Dependencies](#dependencies)
* [Initial_setup](#initial_setup)
* [Python_files_description](#Python_files_description)
* [Usage](#usage)

# OCR_and_ActiveLearning_For_LayoutLM
This task demonstrates the process of applying OCR to document-image and extract information. A simple active learning concept is 
shown by using small subset of OCR generated data with available trained LayoutLM model.

## Prerequisites
This project is tested and works without errors on mac OS and python 3.7

## Dependencies
This project requires

* numpy
* pandas
* matplotlib
* torch
* networkx
* sklearn
* pytesseract
* cv2

## Initial_setup

The FUNSD dataset is used in this and can be downloaded from the following link.(https://guillaumejaume.github.io/FUNSD/)

## Python_files_description

* config.ini

[input]

data = training data path of FUNSD dataset
test_data = test data path of FUNSD dataset

* utils.py :
This file consists of functions to read string , read number and read boolean values. This is used in main.py to read all values sepecified in config.ini.

* Generation_of_Data_OCR.py
This file has two functions, "ocr_preprocessing" to read image and apply OCR to extract the words along with their bounding boxes. This also eliminates few unrecognized symbols by the OCR.
Second function 'filter_certain_ocr_rows' removes certain bounding boxes which doesnot have words and also it considers the bounding boxes which are within specified limits.

* Labels_Appending_for_Data.py
This file contain a function which assigns label for each word extracted using functions defined in "Generation_of_Data_OCR.py". Along with labels it also adds BIESO tagging.

* run_seq_labeling.py :
This file has code for training and evaluation script for given data using layputlm model. The evaluation script is modeified to obtain predictions for each test sample and save with name test_predictions.txt in their respective folders.

* LayoutLM_Input_Files_creation.py
In this file the OCR extracted dataframe is preprocessed according to the input format of LayoutLM model.

  * bbox_string : This function converts bounding box coordinates into relative values according to image width and height.
  * train_txt_file : This function reads in a dataframe and creates a .txt file which has words and corresponding labels.
  * train_image_file : This function reads in a dataframe and creates a .txt file which has word, bounding box coordinates, image width, height and name of an image.
  * train_box_file : It reads in a dataframe and creates a .txt file which has word and corresponding relative bounding box coordinates.
  * main :  This function reads in train data and test data path from config file, three files train.txt, train_image.txt and train_box.txt are created by calling above functions mentioned
   and also by making use of functions mentioned in "Labels_Appending_for_Data.py" and "Generation_of_Data_OCR.py".

## Usage
* Step1 

  Edit the config.ini file according to data path.

* Step2

  Run this command to extract text and bounding box by OCR and prepares data accordingly to LayoutLM.
  ```
  python3 LayoutLM_Input_Files_creation.py -c config.ini
  ```
* Step3

  Run this command to train LayoutLM model with above input files generated.
  ```
  python run_seq_labeling.py  --data_dir '<train_directory_path>'   --model_type layoutlm \
                            --model_name_or_path /content/drive/MyDrive/SushThesis/unilm/layoutlm/layoutlm-base-uncased \
                            --do_lower_case \
                            --max_seq_length 512 \
                            --do_train \
                            --num_train_epochs 1 \
                            --logging_steps 1 \
                            --save_steps -1 \
                            --overwrite_output_dir \
                            --output_dir '<output_directory_path>' \
                            --labels di/labels.txts \
                            --per_gpu_train_batch_size 1 \
                            --per_gpu_eval_batch_size 1 \
                            --seed 12
  ```
* Step4

  Run this command to obtain predictions for test data
  ```
  python run_seq_labeling.py  --data_dir '<test_directory_path>' \
                              --model_type layoutlm \
                              --model_name_or_path '<trained_model_saved_path(output_directory_path in the above training command)>' \
                              --do_lower_case \
                              --max_seq_length 512 \
                              --output_dir '<output_directory_path>' \
                              --labels '<labels.txt file path>' \
                              --per_gpu_eval_batch_size 16 \
                              --do_predict \
                              --seed 12
  ```

NOTE : All the functions incorporated above files are tested and demonstrated their results in Notebooks "OCR_Preprocessing.ipynb" and "Files_For_Layoutlm.ipynb.
To understand the working of each function defined in above files can be witnessed in the above mentioned notebooks.