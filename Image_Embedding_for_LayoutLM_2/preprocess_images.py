import torchvision
import torch
from os import listdir
from PIL import Image
import numpy as np
from torchvision.transforms import ToTensor

def trainImages(trainPath):
	return [f for f in listdir(trainPath)]


def testImages(testPath):
	return [f for f in listdir(testPath)]


def resize_and_align_bounding_box(bbox,image,target_size = 224):
	width_,height_ = image.size

	width_scale = target_size/width_
	height_scale = target_size/height_

	left,top,right,bottom = tuple(bbox)

	width = int(np.round(left * width_scale))
	height = int(np.round(top * height_scale))
	widthMax = int(np.round(right * width_scale))
	heightMax = int(np.round(bottom * height_scale))

	return [width-0.5,height-0.5,widthMax+0.5,heightMax+0.5]


def imageToTensor(resized_image):
	return ToTensor()(resized_image).unsqueeze(0)


def normalize_box(box, width, height):
	return [int(1000 * (box[0]/width)),int(1000*(box[1]/height)),int(1000*(box[2]/width)),int(1000*(box[3]/height))]