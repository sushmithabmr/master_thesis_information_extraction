from torch.utils.data import Dataset, DataLoader
from PIL import Image, ImageDraw, ImageFont
import json
import numpy as np
from torchvision.transforms import ToTensor
from transformers import AdamW
from tqdm.notebook import tqdm
import torch
from preprocess_images import *
from transformers import BertTokenizer
from data import *
from model import *

def iob_to_label(label):
      return label[2:]

def main():

  tokenizer = BertTokenizer.from_pretrained("bert-base-uncased")
  image_files_names = ["83594639.png"]#('/content/drive/MyDrive/SushThesis/Image_Embedding_for_LayoutLM/dataset/testing_data/images/83594639.png')
  inference_dataset = FUNSDDataset(image_file_names=image_files_names, tokenizer=tokenizer, max_length=512, target_size=224, train=False)
  test_encoding = inference_dataset[0]

  device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

  labels = ['B-answer', 'I-answer', 'B-header', 'I-header', 'B-question', 'I-question', 'B-other', 'I-other']
  idx2label = {v: k for v,k in enumerate(labels)}
  label2idx = {k: v for v,k in enumerate(labels)}

  model = LayoutLMForTokenClassification()
  state_dict = torch.load('/content/drive/MyDrive/SushThesis/Image_Embedding_for_LayoutLM/model.pt')
  model.load_state_dict(state_dict)
  #model = torch.load_state_dict(torch.load('/content/drive/MyDrive/SushThesis/Image_Embedding_for_LayoutLM/model.pt'))
  model.to(device)

  for k,v in test_encoding.items():
    test_encoding[k] = test_encoding[k].unsqueeze(0).to(device)

  input_ids=test_encoding['input_ids']
  bbox=test_encoding['bbox']
  attention_mask=test_encoding['attention_mask']
  token_type_ids=test_encoding['token_type_ids']
  labels=test_encoding['labels']
  resized_images = test_encoding['resized_image']
  resized_and_aligned_bounding_boxes = test_encoding['resized_and_aligned_bounding_boxes']

  for k,v in test_encoding.items():
    test_encoding[k] = test_encoding[k].unsqueeze(0).to(device)

  input_ids.to(device)


  outputs = model(input_ids=input_ids, bbox=bbox, attention_mask=attention_mask, token_type_ids=token_type_ids, 
                  labels=labels, resized_images=resized_images, resized_and_aligned_bounding_boxes=resized_and_aligned_bounding_boxes)

  token_predictions = outputs.logits.argmax(-1).squeeze().tolist() # the predictions are at the token level
  token_actual_boxes = test_encoding['unnormalized_token_boxes'].squeeze().tolist()

  word_level_predictions = [] # let's turn them into word level predictions
  final_boxes = []
  for id, token_pred, box in zip(input_ids.squeeze().tolist(), token_predictions, token_actual_boxes):
    if (tokenizer.decode([id]).startswith("##")) or (id in [tokenizer.cls_token_id, 
                                                            tokenizer.sep_token_id, 
                                                            tokenizer.pad_token_id]):
      # skip prediction + bounding box

      continue
    else:
      word_level_predictions.append(token_pred)
      final_boxes.append(box)

  image = Image.open("/content/drive/MyDrive/SushThesis/Image_Embedding_for_LayoutLM/dataset/testing_data/images/83594639.png")
  image = image.convert("RGB")
  draw = ImageDraw.Draw(image)

  font = ImageFont.load_default()


  label2color = {'question':'blue', 'answer':'green', 'header':'orange', 'other':'violet'}

  for prediction, box in zip(word_level_predictions, final_boxes):
      predicted_label = iob_to_label(idx2label[prediction]).lower()
      draw.rectangle(box, outline=label2color[predicted_label])
      draw.text((box[0] + 10, box[1] - 10), text=predicted_label, fill=label2color[predicted_label], font=font)

  image.show()
  image.save('/content/drive/MyDrive/SushThesis/Image_Embedding_for_LayoutLM/dataset/image_predicted.bmp')

  image = Image.open("/content/drive/MyDrive/SushThesis/Image_Embedding_for_LayoutLM/dataset/testing_data/images/83594639.png")
  image = image.convert('RGB')

  draw = ImageDraw.Draw(image)

  label2color = {'question':'blue', 'answer':'green', 'header':'orange', 'other':'violet'}

  with open('/content/drive/MyDrive/SushThesis/Image_Embedding_for_LayoutLM/dataset/testing_data/annotations/83594639.json') as f:
    data = json.load(f)

  for annotation in data['form']:
    label = annotation['label']
    general_box = annotation['box']
    draw.rectangle(general_box, outline=label2color[label], width=2)
    draw.text((general_box[0] + 10, general_box[1] - 10), label, fill=label2color[label], font=font)
    words = annotation['words']
    for word in words:
      box = word['box']
      draw.rectangle(box, outline=label2color[label], width=1)

  image.show()
  image.save('/content/drive/MyDrive/SushThesis/Image_Embedding_for_LayoutLM/dataset/image_gound_truth.bmp')

if __name__ == "__main__" :
 #   print("hello")
  main()