## Table of contents
* [Evaluation_of_each_sample_using_LayoutLM_trained_model](#Evaluation_of_each_sample_using_LayoutLM_trained_model)
* [Prerequisites](#prerequisites)
* [Dependencies](#dependencies)
* [Initial_setup](#initial_setup)
* [Python_files_description](#Python_files_description)
* [Usage](#usage)

# Evaluation_of_each_sample_using_LayoutLM_trained_model
This task involves scripts to evaluate available pretrained LayoutLM model for each test sample and obtain evlauation metric score and visualize the obtained results. 

## Prerequisites
Follow the following steps before going to next steps
This project is tested and works without errors on mac OS and python 2.7


## Dependencies
This project requires
* numpy
* pandas
* matplotlib
* torch
* sklearn
* pytesseract
* PIL
* transformers

## Initial_setup

Download the FUNSD dataset from the website(https://guillaumejaume.github.io/FUNSD/)

## Python_files_description

* config.ini :
This file consists of all input files path which is utilized to run evlautaion script in main.py.

* util.py :
This file consists of functions to read string , read number and read boolean values. This is used in main.py to read all values sepecified in config.ini.

* main.py :
This file contains code to run evaluations for each test sample. It creates separate folder for each test sample, which contains test sample image and respective annotation. It also contains command to run preprocessing script for test samples and command to run evaluation script for test samples.

* preprocess_test.py :
This file has code to generate truevalues(test.txt), words with their actual bounding boxes and image cordinates(test_image.txt) and words with their normalized bounding boxes values(test_bbox.txt) files from each test sample image with their respective annotations, which are required for layoutlm model for predictions.

* run_seq_labeling_test.py :
This file has code for training and evaluation script for given data using layputlm model. The evaluation script is modeified to obtain predictions for each test sample and save with name test_predictions.txt in theior respective folders.

* ImageLabel.py:
This file has code to visualize the predictions in comparision with trues for each sample.

## Usage
* Step1 

  Edit the config.ini file according to data path and model path present.

* Step2 

  Edit the main.py for preprocessing script and evaluation script commands present according to test data.

* Step3 

  Run this command to run preprocess and evaluation scripts for each test sample
  ```
  "python3 main.py -c config.ini"
  ```

* Step4 

  Followed by this command to visualize the results
  ```
  "python3 ImageLabel.py -c config.ini"
  ```

Optional: 

THESE ABOVE STEPS HAVE BEEN FOLLOWED IN THE NOTEBOOK "Notebooks/FUNSD_WeakPredictions.ipynb" and "intermediate_Training_and_Evaluation.ipynb". 
THE DETAILED DESCRIPTION ABOUT LIBRARIES TO BE INSTALLED AND THE PROCESS TO OBTAIN EVALUATION RESULTS FOR EACH SAMPLE USING LAYOUTLM HAS BEEN ADDED TO THE NOTEBOOKS .
PLEASE REFER NOTEBOOKS AT FIRST.
