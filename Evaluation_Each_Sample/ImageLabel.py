#!/usr/bin/python
# -*- coding: utf-8 -*-
import os
import csv
import sys
import shutil
import subprocess
import configparser
import argparse
import util
import json
import numpy as np
import pandas as pd
from PIL import Image, ImageDraw, ImageFont
import cv2
#from google.colab.patches import cv2_imshow


def main():
    
    # set up a parser
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--config", required=False, default="config.ini", help="Config file")

    # parse arguments
    args = parser.parse_args()
    cfg = args.config
    cfgparser = configparser.ConfigParser()
    res = cfgparser.read(cfg)
    if len(res) == 0:
        print("Error: None of the config files could be read")
        sys.exit(1)

    # read the config
    test_data_path = util.read_cfg_string(cfgparser, 'input', 'test_data_path', default=None)
    test_data_split = util.read_cfg_string(cfgparser, 'input', 'test_data_split', default='test')
    test_output_folder = util.read_cfg_string(cfgparser, 'output', 'test_output_folder', default='test_output_folder')
    
    
    
    len1 = os.listdir(os.path.join(test_data_path,'train'))
    print(len1)
    itercars = iter(os.walk(os.path.join(test_data_path,test_data_split)))
    next(itercars)
    for subdir, dirs, files in itercars:
        #print(subdir)
        df_predictions = pd.read_csv(os.path.join(subdir,'test_predictions.txt'), delimiter = " ", header=None, names=['text_predictions','label_predictions'], engine='python', quoting=csv.QUOTE_NONE)
        df_trueValues = pd.read_csv(os.path.join(subdir,'test.txt'), delimiter="\t",header=None, names=['text_true','label_true'], engine='python', quoting=csv.QUOTE_NONE)
        df_bbox = pd.read_csv(os.path.join(subdir,'test_image.txt'), delimiter="\t",header=None, engine='python', quoting=csv.QUOTE_NONE)
        df_bbox[['bbox1','bbox2','bbox3','bbox4']] = df_bbox[1].str.split(' ', expand = True)
        df_bbox[['img_width','img_length']] = df_bbox[2].str.split(' ', expand = True)
        df_bbox.drop(1, axis=1, inplace=True)
        df_bbox.rename({0: 'text', }, axis=1, inplace=True)
        match_index = []
        non_match_index = []
        for i in df_predictions['text_predictions'].index:
            for j in df_trueValues['text_true'].index:
                if (df_predictions['text_predictions'][i] == df_trueValues['text_true'][j] and i==j):
                    if ( df_predictions['label_predictions'][i] != df_trueValues['label_true'][j]):
                        non_match_index.append(j)
                    if (df_predictions['label_predictions'][i] == df_trueValues['label_true'][j]):
                        match_index.append(j)
            
        font = ImageFont.load_default()
        #font = ImageFont.truetype('Tests/fonts/DejaVuSans.ttf')
        #font = ImageFont.truetype(font=font, size=10, index=0, encoding="unic")
        fol = subdir.split('/')[-1]
        image1 = Image.open(os.path.join(subdir,fol+'.png'))
        image1 = image1.convert('RGB')

        draw1 = ImageDraw.Draw(image1)

        for num in match_index:
            if (df_predictions['text_predictions'][num] == df_bbox['text'][num]):
                bbox = [df_bbox['bbox1'][num],df_bbox['bbox2'][num],df_bbox['bbox3'][num],df_bbox['bbox4'][num]]
                bbox = list(map(int, bbox))
                draw1.rectangle(bbox, outline= "green", width=2)
                draw1.text((bbox[0] + 10, bbox[1] - 10), df_predictions['text_predictions'][num].encode('utf-8') , fill ="green", font=font)

        for num in non_match_index:
            if (df_predictions['text_predictions'][num] == df_bbox['text'][num]):
                bbox = [df_bbox['bbox1'][num],df_bbox['bbox2'][num],df_bbox['bbox3'][num],df_bbox['bbox4'][num]]
                bbox = list(map(int, bbox))
                draw1.rectangle(bbox, outline= "red", width=2)
                draw1.text((bbox[0] + 10, bbox[1] - 10), df_predictions['text_predictions'][num].encode('utf-8') , fill ="red", font=font)

        image1.save(os.path.join(subdir,'result.png'))
    print("All images saved")
        
        
    
    # To show images which has f1 score of weak predictions 
    iterweak = iter(os.walk(os.path.join(test_data_path,test_data_split)))
    next(iterweak)
    for subdir, dirs, files in iterweak:
        fol = subdir.split('/')[-1]
        test_results = pd.read_csv(os.path.join(subdir,'test_results.txt'), delimiter=" = ",names =['metric','score'], engine='python')
        if (test_results['score'][0] <=0.99):
            print('F1 Score of '+fol+' : ',test_results['score'][0])
            #image = cv2.imread(os.path.join(subdir,'result.png'))
            #cv2.imshow('Final Results',image)
            im = Image.open(os.path.join(subdir,'result.png'))  
            im.show() 
            
if __name__ == "__main__" :
    main()
 
