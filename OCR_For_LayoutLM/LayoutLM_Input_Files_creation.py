# -*- coding: utf-8 -*-
import os
import csv
import sys
import shutil
import subprocess
import configparser
import argparse
import json
import numpy as np
import pandas as pd
from PIL import Image, ImageDraw, ImageFont
import cv2

#!/usr/bin/python
import os
import sys
import shutil
import subprocess
import configparser
import argparse
import util
import json
import numpy as np


from pdf2image import convert_from_path
import pytesseract
from pytesseract import Output

pd.set_option("max_rows", 500)
pd.set_option('max_columns', 100)

import utils
from Labels_Appending_for_Data import temp_file_creation
from Generation_of_Data_OCR import ocr_preprocessing, filter_certain_ocr_rows



def bbox_string(left, top, n_width, n_height, width_img, height_img):
    return (
        str(int(1000 * (left / width_img)))
        + " "
        + str(int(1000 * (top / height_img)))
        + " "
        + str(int(1000 * (n_width / width_img)))
        + " "
        + str(int(1000 * (n_height / height_img)))
    )


def train_txt_file(train_file_object, text, labels):
    data_tuples = list(zip(text, labels))
    sorted_data = pd.DataFrame(data_tuples, columns=['text', 'label'])
    train_file_object.write(sorted_data.to_string(header=False, index=False)+ "\n")
    train_file_object.write("\n")
    print('train.txt file created')


def train_image_file(train_image_file_object, final_ocr):
    train_image = final_ocr[['text', 'left', 'top', 'n_width', 'n_height', 'width_img', 'height_img', 'name']]
    # works the best
    train_image_file_object.write(train_image.to_string(header=False, index=False) + "\n")
    train_image_file_object.write("\n")
    print('train_image.txt file created')


def train_box_file(train_box_file_object, final_ocr):
    train_data = final_ocr[['text', 'left', 'top', 'n_width', 'n_height', 'width_img', 'height_img', 'name']]
    text = train_data['text'].tolist()
    data_bbox_relative_list = []
    for index, row in train_data.iterrows():
        boxValues = bbox_string(row['left'], row['top'], row['n_width'], row['n_height'], row['width_img'], row['height_img'])
        data_bbox_relative_list.append(boxValues)
    train_box = pd.DataFrame({'text': text, 'bbox': data_bbox_relative_list})
    # works the best
    train_box_file_object.write(train_box.to_string(header=False, index=False) + "\n")
    train_box_file_object.write("\n")
    print('train_box.txt file is created')


def main():

    # set up a parser
    parser = argparse.ArgumentParser()
    print("hello")
    parser.add_argument("-c", "--config", required=False, default="config.ini", help="Config file")

    # parse arguments
    args = parser.parse_args()
    cfg = args.config
    cfgparser = configparser.ConfigParser()
    res = cfgparser.read(cfg)
    if len(res) == 0:
        print("Error: None of the config files could be read")
        sys.exit(1)

    # read the config
    data_dir = util.read_cfg_string(cfgparser, 'input', 'data', default=None)
    test_data_dir = util.read_cfg_string(cfgparser, 'input', 'test_data', default=None)


    # train.txt and train_image.txt and train_box.txt files creation for training 15 documents 
    with open(os.path.join(data_dir, "train.txt"),"w",encoding="utf8",) as ft,open(os.path.join(data_dir, "train_image.txt"),"w",encoding="utf8",) as fti,open(os.path.join(data_dir, "train_box.txt"),"w",encoding="utf8",) as ftb:
        for file in os.listdir(data_dir):
            print(file) 
            file_path = os.path.join(data_dir, file, file+'.json')
            print(file_path)
            with open(file_path, "r", encoding="utf8") as fj:
                data = json.load(fj)
            #image_path = file_path.replace("annotations", "images")
            image_path = file_path.replace("json", "png")
            file_name = os.path.basename(image_path)
            print('filename', file_name)
            image = Image.open(image_path)
            #pytessaract ocr
            ocr = ocr_preprocessing(image_path)
            #temp file creation
            temp = temp_file_creation(file_path)
            #print('ocr', ocr)
            #print('temp', temp)
            #filter certain rows based on temp dataframe and ocr dataframe
            final_ocr, text, labels, indexes = filter_certain_ocr_rows(ocr, temp)
            #train_txt file creation
            train_txt_file(ft, text, labels)
            #train_image_txt file creation  
            train_image_file(fti, final_ocr)
            #train_box_txt file creation
            train_box_file(ftb, final_ocr)
            print('files are creating')

    # test.txt and test_image.txt and test_box.txt files creation for training 109 documents
    with open(os.path.join(test_data_dir, "train.txt"),"w",encoding="utf8",) as ft,open(os.path.join(test_data_dir,"train_image.txt"),"w",encoding="utf8",) as fti,open(os.path.join(test_data_dir,"train_box.txt"),"w",encoding="utf8",) as ftb:
        for file in os.listdir(test_data_dir):
            print(file) 
            file_path = os.path.join(test_data_dir, file, file+'.json')
            print(file_path)
            with open(file_path, "r", encoding="utf8") as fj:
                data = json.load(fj)
            #image_path = file_path.replace("annotations", "images")
            image_path = file_path.replace("json", "png")
            file_name = os.path.basename(image_path)
            print('filename', file_name)
            image = Image.open(image_path)
            #pytessaract ocr
            ocr = ocr_preprocessing(image_path)
            #temp file creation
            temp = temp_file_creation(file_path)
            #print('ocr', ocr)
            #print('temp', temp)
            #filter certain rows based on temp dataframe and ocr dataframe
            final_ocr, text, labels, indexes = filter_certain_ocr_rows(ocr, temp)
            #train_txt file creation
            train_txt_file(ft, text, labels)
            #train_image_txt file creation  
            train_image_file(fti, final_ocr)
            #train_box_txt file creation
            train_box_file(ftb, final_ocr)
            print('files are creating')


if __name__ == "__main__":
    # print("hello")
    main()
