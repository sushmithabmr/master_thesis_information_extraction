# -*- coding: utf-8 -*-
import os
import csv
import sys
import shutil
import subprocess
import configparser
import argparse
import json
import numpy as np
import pandas as pd
from PIL import Image, ImageDraw, ImageFont
import cv2


from pdf2image import convert_from_path
import pytesseract
from pytesseract import Output

pd.set_option("max_rows", 500)
pd.set_option('max_columns', 100)


def ocr_preprocessing(image_path):

  image = Image.open(image_path)
  width, height = image.size
  w_scale = 1000/width
  h_scale = 1000/height
  ocr_df = pytesseract.image_to_data(image, output_type='data.frame')
  #print(ocr_df)
  filename = os.path.basename(image_path)
  ocr_df['name'] = filename
  ocr_df['width_img'] = width
  ocr_df['height_img'] = height
  ocr_df['n_width'] = ocr_df['left']+ocr_df['width']
  ocr_df['n_height'] = ocr_df['top']+ocr_df['height']
  ocr_df['bbox'] = ocr_df[["left", "top", "n_width", "n_height"]].values.tolist()
  ocr_na = ocr_df.dropna()
  #print(ocr_na)
  #np.savetxt('/content/some.txt', ocr_na.values, fmt='%s', delimiter='\t')
  float_cols = ocr_na.select_dtypes('float').columns
  ocr_na[float_cols] = ocr_na[float_cols].round(0).astype(int)
  #print(str(ocr_na.loc[4, 'text']))
  for index,row in ocr_na.iterrows():
    #print(row['text'])
    if(row['text'] == ' '): # eliminate unrecognised symbols 
      #print('empty space',index)
      ocr_na = ocr_na.drop(index)
    if(row['top']== 0):   # drop one big blounding box applied to entire image
      #print('image size bbox',index)
      ocr_na = ocr_na.drop(index)
  return ocr_na


def filter_certain_ocr_rows(ocr_dataframe, temp_dataframe):
  ocr_index_list = []
  list1 = []
  list2 = []
  list3 = []
  for index,row in ocr_dataframe.iterrows():
    for index2,row2 in temp_dataframe.iterrows():
      if((row2['text'] == row['text']) and ((int(row2["left"])-10)<row["left"]<(int(row2["left"])+10)) and ((int(row2["top"])-10)<row["top"]<(int(row2["top"])+10))):
        l = row['text']
        list1.append(l)
        l1 = row2["label"]
        list2.append(l1)
        l3 = index
        list3.append(l3)
        ocr_index_list.append(index)

  ocr_index_list.sort()
  final_ocr = ocr_dataframe.loc[ocr_index_list]
  final_ocr = final_ocr.reset_index()
  return final_ocr, list1, list2, list3