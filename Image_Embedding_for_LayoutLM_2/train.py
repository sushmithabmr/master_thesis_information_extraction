from torch.utils.data import Dataset, DataLoader
from PIL import Image
import json
import numpy as np
from torchvision.transforms import ToTensor
from transformers import AdamW
from tqdm.notebook import tqdm
import torch
from preprocess_images import *
from transformers import BertTokenizer
from data import *
from model import *

def main():

	tokenizer  = BertTokenizer.from_pretrained("bert-base-uncased", do_lower_case = True)
	image_files_train = [f for f in listdir('/content/drive/MyDrive/SushThesis/Image_Embedding_for_LayoutLM/dataset/training_data/images')]
	train_dataset = FUNSDDataset(image_file_names=image_files_train, tokenizer=tokenizer, max_length=512, target_size=224)
	train_dataloader = DataLoader(train_dataset, batch_size = 4)
	#batch = next(iter(train_dataloader))

	model = LayoutLMForTokenClassification()

	optimizer = AdamW(model.parameters(),lr = 5e-5)

	global_step = 0
	epochs = 20

	device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

	model.to(device)
	model.train()
	for epoch in range(epochs):
		print("Epoch:", epoch)
		for batch in tqdm(train_dataloader):
			# forward pass
			input_ids = batch['input_ids'].to(device)
			bbox = batch['bbox'].to(device)
			attention_mask = batch['attention_mask'].to(device)
			token_type_ids = batch['token_type_ids'].to(device)
			labels = batch['labels'].to(device)
			resized_images = batch['resized_image'].to(device)
			resized_and_aligned_bounding_boxes = batch['resized_and_aligned_bounding_boxes'].to(device)

			outputs = model(input_ids=input_ids, bbox=bbox, attention_mask=attention_mask, token_type_ids=token_type_ids, 
						labels=labels, resized_images=resized_images, resized_and_aligned_bounding_boxes=resized_and_aligned_bounding_boxes)

			loss = outputs.loss

			if global_step % 10 == 0:
				print(f"Loss after {global_step} steps: {loss.item()}")

			loss.backward()

			optimizer.step()
			optimizer.zero_grad()
			global_step+=1

	torch.save(model.state_dict(), '/content/drive/MyDrive/SushThesis/Image_Embedding_for_LayoutLM/model.pt')
	print('saved model')

if __name__ == "__main__" :
 #   print("hello")
    main()