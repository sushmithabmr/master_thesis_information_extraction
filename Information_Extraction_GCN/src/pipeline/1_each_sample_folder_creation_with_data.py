#!/usr/bin/python
import os
import sys
import shutil
import subprocess
import configparser
import argparse
import util
import json
import numpy as np


def main():

    # set up a parser
    parser = argparse.ArgumentParser()
    print("1. Folder creation for each sample and preprocess respective annotations to generate test.txt, test_image.txt and test_bbox.txt files")
    parser.add_argument("-c", "--config", required=False, default="config.ini", help="Config file")

    # parse arguments
    args = parser.parse_args()
    cfg = args.config
    cfgparser = configparser.ConfigParser()
    res = cfgparser.read(cfg)
    if len(res) == 0:
        print("Error: None of the config files could be read")
        sys.exit(1)

    # read the config
    # Raw data prepared only for training data in the FUNSD actual dataset. 
    # The same can be performed to also testing_data folder.
    train_data_path = util.read_cfg_string(cfgparser, 'input', 'train_data_path', default=None)
    # test_data_path = util.read_cfg_string(cfgparser, 'input', 'test_data_path', default=None)


    # To read each sample image from folder
    samples = os.listdir(os.path.join(train_data_path, 'images'))

    # To create each_sample_folder which containes separate folder for each sample
    if not os.path.exists(os.path.join(train_data_path, 'each_sample_folder')):
        os.mkdir(os.path.join(train_data_path, 'each_sample_folder'))
    else:
        for num in range(len(samples)):
            # fetch each sample name
            name = samples[num].split('.')[0]
            # create separate folder for each sample to store respective related files
            os.mkdir(os.path.join(train_data_path,'each_sample_folder',name))
            # fetch each sample image path
            src = os.path.join(train_data_path,"images",name+".png")
            # fetch each sample annotations path
            src_anno = os.path.join(train_data_path,"annotations",name+".json")
            # define destination paths for both image and annotaions for each sample respective folders
            dest = os.path.join(train_data_path,'each_sample_folder',name,name+".png")
            dest_anno = os.path.join(train_data_path,'each_sample_folder',name,name+".json")
            # To move each image from images folder to their respective folder
            shutil.copyfile(src,dest)
            # To move each annotation of respective image found in images folder to their respective folder
            shutil.copyfile(src_anno,dest_anno)

        # Run preprocessing script for each sample data to create test.txt, test_image.txt and test_bbox.txt files by extractiing data from annotations.
        os.system("python raw_data_info_extraction_files_creation.py --data_dir '/Users/sushmitha/SushThesis/info_extraction_receipts/data/FUNSD/training_data/each_sample_folder/' --data_split train --output_dir '/Users/sushmitha/SushThesis/info_extraction_receipts/data/FUNSD/training_data/each_sample_folder/' --model_name_or_path bert-base-uncased --max_len 510")


if __name__ == "__main__":
    main()