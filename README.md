# Towards Understanding of Scanned Business Documents : Automatic Information Extraction Through Multi-modal Encodings

Information Extraction: The task of automatically extracting structured information from unstructured and/or semi-structured machine-readable texts is known as information extraction (IE).

For supervised learning, the pre-trained NLP and CV models each integrate input from various modalities. 
Even if higher performance has been obtained, when the document type is changed, these models frequently need to be retrained from start. Furthermore, domain information from one document type cannot readily be transferred to another, limiting the use of local invariance in general document layout (e.g. key-value pairs in a left-right layout, tables in a grid layout, etc). 
The second approach depends on deep fusion of textual, visual, and layout information from a large number of unlabeled texts from many areas to achieve this goal.

The following objectives are defined in this thesis work.

* To explore the LayoutLM Base model for form understanding using datasets which include a variety of scanned document scans containing textual information represented by text embeddings and position embeddings.
* Document pre-processing by applying OCR to document images to obtain both the recognised words and their corresponding locations in the document images. A simple active learning concept for labelling a large amount of OCR generated data by providing a small subset of labelled OCR generated data with available pre-trained LayoutLM model.
* An addition of image embedding layer to LayoutLM base model to incorporate word's visual information by representing the image region features with the pieces of images as token image embeddings.
* Implementation of alternative approach of leveraging text, image and position features of document images by using Graph Convolution Networks because to catch the relationship between nodes(words) within a document and to get a richer graph embeddings representation of nodes(words) within a document.
* Experiments on FUNSD dataset by evaluating of pre-trained LayoutLM model which consists of text embedding layer and position embeddings layer.
* Experiments on evaluating LayoutLM Base model against LayoutLM + Image Embeddings model to show that adding the image region features outperforms base model.
* Evaluating the performance of LayoutLM base model, LayoutLM + Image Embeddings model against Graph Convolution Networks model in extracting information in form understanding task by making use of visually rich scanned documents. 

This master thesis work involves 4 tasks, which are listed below and their corresponding scripts, steps to run their scripts are found in their respective folders.
1. Evaluation_Each_Sample
  This task involves scripts to evaluate available pretrained LayoutLM model for each test sample and obtain evlauation metric score and visualize the obtained results. 
 
2. OCR_For_LayoutLM
  This task demonstrates the process of applying OCR to document-image and extract information. A simple active learning concept is 
shown by using small subset of OCR generated data with available trained LayoutLM model.

3. Image_Embedding_for_LayoutLM 2
  To fine-tune LayoutLM on the FUNSD dataset, and add visual features to LayoutLM from a pre-trained backbone (ResNet-101). 

4. Information_Extraction_GCN
  This project contains python scripts to generate training data for Graph convolution networks (GCN). 