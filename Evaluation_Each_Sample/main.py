#!/usr/bin/python
import os
import sys
import shutil
import subprocess
import configparser
import argparse
import util
import json
import numpy as np


def main():

    # set up a parser
    parser = argparse.ArgumentParser()
    print("hello")
    parser.add_argument("-c", "--config", required=False, default="config.ini", help="Config file")

    # parse arguments
    args = parser.parse_args()
    cfg = args.config
    cfgparser = configparser.ConfigParser()
    res = cfgparser.read(cfg)
    if len(res) == 0:
        print("Error: None of the config files could be read")
        sys.exit(1)

    # read the config
    test_data_path = util.read_cfg_string(cfgparser, 'input', 'test_data_path', default=None)
    test_data_split = util.read_cfg_string(cfgparser, 'input', 'test_data_split', default='train')
    test_model_name_or_path = util.read_cfg_string(cfgparser, 'input', 'test_model_name_or_path', default='bert-base-uncased')
    test_output_folder = util.read_cfg_string(cfgparser, 'output', 'test_output_folder', default='test_output_folder')
    test_preprocess_script = util.read_cfg_string(cfgparser,'input','test_preprocess_script',default=None)


    # Following code represents to create different folders for each test sample and move each sample to their respective folders for evaluation
    len1 = os.listdir(os.path.join(test_data_path,'images'))
    os.mkdir(os.path.join(test_data_path,'train'))
    for num in range(len(len1)):
        name = len1[num].split('.')[0]
        os.mkdir(os.path.join(test_data_path,test_data_split,name))
        src = os.path.join(test_data_path,"images",name+".png")
        src_anno = os.path.join(test_data_path,"annotations",name+".json")
        dest = os.path.join(test_data_path,test_data_split,name,name+".png")
        dest_anno = os.path.join(test_data_path,test_data_split,name,name+".json")
        # To move each image from images folder to their respective folder
        shutil.copyfile(src,dest)
        # To move each annotation of respective image found in images folder to their respective folder
        shutil.copyfile(src_anno,dest_anno)

    # Run prprocessing script for each test data
    os.system("python preprocess_test.py --data_dir '/content/drive/MyDrive/SushThesis/unilm/layoutlm/examples/seq_labeling/dataset_full/training_data/train/' --data_split train --output_dir '/content/drive/MyDrive/SushThesis/unilm/layoutlm/examples/seq_labeling/dataset_full/training_data/train/' --model_name_or_path bert-base-uncased --max_len 510")

    # Run evaluation script for each test data
    os.system("! python run_seq_labeling.py  --data_dir '/content/drive/MyDrive/SushThesis/unilm/layoutlm/examples/seq_labeling/dataset_full/testing_data/test/' --model_type layoutlm --model_name_or_path '/content/drive/MyDrive/SushThesis/unilm/layoutlm/examples/seq_labeling/output' --do_lower_case --max_seq_length 512 --output_dir '/content/drive/MyDrive/SushThesis/unilm/layoutlm/examples/seq_labeling/output'--labels '/content/drive/MyDrive/SushThesis/unilm/layoutlm/examples/seq_labeling/data/labels.txts' --per_gpu_eval_batch_size 16 --do_predict--seed 12")

    
if __name__ == "__main__" :
 #   print("hello")
    main()
 
 
    
