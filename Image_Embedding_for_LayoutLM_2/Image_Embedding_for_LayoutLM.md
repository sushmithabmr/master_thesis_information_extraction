
## Table of contents
* [Image_Embedding_for_LayoutLM](#Image_Embedding_for_LayoutLM)
* [Prerequisites](#prerequisites)
* [Dependencies](#dependencies)
* [Initial_setup](#initial_setup)
* [Python_files_description](#Python_files_description)
* [Usage](#usage)

# Image_Embedding_for_LayoutLM
To fine-tune LayoutLM on the FUNSD dataset, and add visual features to LayoutLM from a pre-trained backbone (ResNet-101). 

## Prerequisites
Follow the following steps before going to next steps
This project is tested and works without errors on mac OS and python 3.7

## Dependencies
This project requires
* numpy
* pandas
* matplotlib
* torch
* pytesseract
* cv2
* transfromers
* Seqeval
* pdf2image
* torchvision

## Python_files_description
* data.py

Class FUNSDDataset constructs the data for LayoutLM+ImageEmbedding model by using its bounding boxes and labels.
Each training sample has image and annotations file consisiting of words with corresponding bounding boxes and sequence labels for the given document.
Resize the image to 224x224 in order to feed it to a pre-trained ResNet-101 CNN, bounding boxes are resized accordingly.
Convert everything to pytorch tensors and returns the encodings. 

* preprocess_images.py

It has helper funsctions to preprocess dataset, which are used in FUNSDDataset class.
This file contains functions to resize and align bounding box to feed to pretrained model and normalize box to adjust the bounding box coordinates.

* model.py

class LayoutLMForTokenClassification is constructed to combine LayoutLM+ImageEmbedding pytorch model. 
Pretrained LayputLM model is used to extract text embeddings, ResNet101 has pretrained backbone model + pytorch ROI align to extract visual embeddings.
A linear projection layer is used to match the dimensions of layoutLM. 

* train.py

Train dataset path is set here and is prprocssed using FUNSDDataset class defiend in 'dataset.py'. LayoutLM+ImageEmbedding model is instatiated, which is defined in 'model.py'. 
All the training parameters are set and combined model is trained, also this trained model is saved in the specified path.

* test.py

The testing data path is set here and is prprocssed using FUNSDDataset class defiend in 'dataset.py'. The trained model is loaded and is evaluated on test data. 
The extracted predictions are computed and appended with labels. Finally evaluation metrics like F1 score, precision and recall are calculated here.

* test_inference.py

To get an inference on one image by using trained model is performed in the file. The predictions extracted are visualized on an image by defininf different color for every
class along with their bounding boxes. 

Note: The usage of every file has been shown in the notebook "Modularized_Image_Embedding.ipynb"

## Usage
* Step1 

  Set the FUNSD training dataste path in "train.py" file.

* Step2 

  Define the training parameters in "train.py".

* Step3 

  Run this command in the notebook "Modularized_Image_Embedding.ipynb" to train and save the trained model.
  ```
  "python3 train.py"
  ```

* Step4 

  Followed by this command in the notebook "Modularized_Image_Embedding.ipynb" to evaluate model with test data.
  ```
  "python3 test.py"
  ```

* Step5 

  Followed by this command in the notebook "Modularized_Image_Embedding.ipynb" to visualize the results extracted from trained model on Image.
  ```
  "python3 test_inference.py"
  ```