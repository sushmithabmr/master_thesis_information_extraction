from torch.utils.data import Dataset, DataLoader
from PIL import Image
import json
import numpy as np
from torchvision.transforms import ToTensor
import torch
from preprocess_images import *
from transformers import BertTokenizer


class FUNSDDataset(Dataset):
    def __init__(self, image_file_names, tokenizer, max_length, target_size, train=True):
        self.image_file_names = image_file_names
        self.tokenizer = tokenizer
        self.max_seq_length = max_length
        self.target_size = target_size
        self.pad_token_box = [0, 0, 0, 0]
        self.train = train

    def __len__(self):
        return len(self.image_file_names)

    def __getitem__(self, idx):

        labels = ['B-answer', 'I-answer', 'B-header', 'I-header', 'B-question', 'I-question', 'B-other', 'I-other']
        idx2label = {v: k for v, k in enumerate(labels)}
        label2idx = {k: v for v, k in enumerate(labels)}

        # first, take an image
        item = self.image_file_names[idx]
        if self.train:
          base_path = "/content/drive/MyDrive/SushThesis/Image_Embedding_for_LayoutLM/dataset/training_data"
        else:
          base_path = "/content/drive/MyDrive/SushThesis/Image_Embedding_for_LayoutLM/dataset/testing_data"

        original_image = Image.open(base_path + "/images/" + item).convert("RGB")
        # resize to target size (to be provided to the pre-trained backbone)
        resized_image = original_image.resize((self.target_size, self.target_size))

        # first, read in annotations at word-level (words, bounding boxes, labels)
        with open(base_path + '/annotations/' + item[:-4] + '.json') as f:
          data = json.load(f)
        words = []
        unnormalized_word_boxes = []
        word_labels = []
        for annotation in data['form']:
          # get label
          label = annotation['label']
          # get words
          for annotated_word in annotation['words']:
              if annotated_word['text'] == '':
                continue
              words.append(annotated_word['text'])
              unnormalized_word_boxes.append(annotated_word['box'])
              word_labels.append(label)

        width, height = original_image.size
        normalized_word_boxes = [normalize_box(bbox, width, height) for bbox in unnormalized_word_boxes]
        assert len(words) == len(normalized_word_boxes)

        # next, transform to token-level (input_ids, attention_mask, token_type_ids, bbox, labels)
        token_boxes = []
        unnormalized_token_boxes = []
        token_labels = []
        for word, unnormalized_box, box, label in zip(words, unnormalized_word_boxes, normalized_word_boxes, word_labels):
            word_tokens = self.tokenizer.tokenize(word)
            unnormalized_token_boxes.extend(unnormalized_box for _ in range(len(word_tokens)))
            token_boxes.extend(box for _ in range(len(word_tokens)))
            # label first token as B-label (beginning), label all remaining tokens as I-label (inside)
            for i in range(len(word_tokens)):
              if i == 0:
                token_labels.extend(['B-' + label])
              else:
                token_labels.extend(['I-' + label])

        # Truncation of token_boxes + token_labels
        special_tokens_count = 2 
        if len(token_boxes) > self.max_seq_length - special_tokens_count:
            token_boxes = token_boxes[: (self.max_seq_length - special_tokens_count)]
            unnormalized_token_boxes = unnormalized_token_boxes[: (self.max_seq_length - special_tokens_count)]
            token_labels = token_labels[: (self.max_seq_length - special_tokens_count)]

        # add bounding boxes and labels of cls + sep tokens
        token_boxes = [[0, 0, 0, 0]] + token_boxes + [[1000, 1000, 1000, 1000]]
        unnormalized_token_boxes = [[0, 0, 0, 0]] + unnormalized_token_boxes + [[1000, 1000, 1000, 1000]]
        token_labels = [-100] + token_labels + [-100]

        encoding = self.tokenizer(' '.join(words), padding='max_length', truncation=True)
        # Padding of token_boxes up the bounding boxes to the sequence length.
        input_ids = self.tokenizer(' '.join(words), truncation=True)["input_ids"]
        padding_length = self.max_seq_length - len(input_ids)
        token_boxes += [self.pad_token_box] * padding_length
        unnormalized_token_boxes += [self.pad_token_box] * padding_length
        token_labels += [-100] * padding_length
        encoding['bbox'] = token_boxes
        encoding['labels'] = token_labels

        assert len(encoding['input_ids']) == self.max_seq_length
        assert len(encoding['attention_mask']) == self.max_seq_length
        assert len(encoding['token_type_ids']) == self.max_seq_length
        assert len(encoding['bbox']) == self.max_seq_length
        assert len(encoding['labels']) == self.max_seq_length

        encoding['resized_image'] = ToTensor()(resized_image)
        # rescale and align the bounding boxes to match the resized image size (typically 224x224) 
        encoding['resized_and_aligned_bounding_boxes'] = [resize_and_align_bounding_box(bbox, original_image, self.target_size) 
                                                          for bbox in unnormalized_token_boxes]

        encoding['unnormalized_token_boxes'] = unnormalized_token_boxes

        # finally, convert everything to PyTorch tensors 
        for k,v in encoding.items():
            if k == 'labels':
              label_indices = []
              # convert labels from string to indices
              for label in encoding[k]:
                if label != -100:
                  label_indices.append(label2idx[label])
                else:
                  label_indices.append(label)
              encoding[k] = label_indices
            encoding[k] = torch.as_tensor(encoding[k])

        return encoding
