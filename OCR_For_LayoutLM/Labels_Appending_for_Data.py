# -*- coding: utf-8 -*-
import os
import csv
import sys
import shutil
import subprocess
import configparser
import argparse
import json
import numpy as np
import pandas as pd
from PIL import Image, ImageDraw, ImageFont
import cv2


from pdf2image import convert_from_path
import pytesseract
from pytesseract import Output

pd.set_option("max_rows", 500)
pd.set_option('max_columns', 100)


def left(box):
  return str(box[0])

def top(box):
  return str(box[1])

def temp_file_creation(file_path):
    with open(file_path, "r", encoding="utf8") as f:
      label_file = json.load(f)
    temp = pd.DataFrame(columns=['text', 'left', 'top', 'label'])
    for item in label_file["form"]:
        words, label = item["words"], item["label"]
        words = [w for w in words if w["text"].strip() != ""]
        #print(words)
        if len(words) == 0:
          continue
        if label == "other":
          for w in words:
            left1 = left(w["box"])
            top1 = top(w["box"])
            temp = temp.append({'text': w["text"], 'left': left1, 'top': top1, 'label': 'O'}, ignore_index=True)
        else:
          if len(words) == 1:
            left2 = left(words[0]["box"])
            top2 = top(words[0]["box"])
            label2 = 'S-'+ label.upper()
            temp = temp.append({'text': words[0]["text"], 'left': left2, 'top': top2, 'label': label2}, ignore_index=True)
          else:
            left3 = left(words[0]["box"])
            top3 = top(words[0]["box"])
            label3 = 'B-'+ label.upper()
            temp = temp.append({'text': words[0]["text"], 'left': left3, 'top': top3, 'label': label3}, ignore_index=True)
            for w in words[1:-1]:
              left4 = left(w["box"])
              top4 = top(w["box"])
              label4 = 'I-'+ label.upper()
              temp = temp.append({'text': w["text"], 'left': left4, 'top': top4, 'label': label4}, ignore_index=True)
            left5 = left(words[-1]["box"])
            top5 = top(words[-1]["box"])
            label5 = 'E-'+ label.upper()
            temp = temp.append({'text': words[-1]["text"], 'left': left5, 'top': top5, 'label': label5}, ignore_index=True)
    print('temp file created')
    #print(temp.dtypes)
    return temp